# filter OMDv1 for surface ocean

# reviewers suggest comparison with other surfaces
# in the open ocean, this is difficult since only plastics float
# alternatively, biofilm on glass slides could be used as comparison
# I would exclude natural substrate and ceramic from the comparison because
# those offer microniches that are not comparable with a plastic surface

setwd("C:/Users/chassenrueck/Documents/IOW_home/IOWseq_projects/IOWseq000039_GPGP_wholegenomes/OMDv1_comparison/")
require(tidyverse)
require(janitor)
require(data.table)

genome_data <- read.table(
  "genomes-summary.csv",
  h = T, sep = ",",
  quote = "\""
) %>% 
  clean_names()

marref <- read.table("MarRef_1.7.tsv", h = T, sep = "\t", quote = "", comment.char = "") %>% 
  clean_names() %>% 
  mutate(mar_id = gsub("^.*:", "", id))

mardb <- read.table("MarDB_1.6.tsv", h = T, sep = "\t", quote = "", comment.char = "") %>% 
  clean_names() %>% 
  mutate(mar_id = gsub("^.*:", "", id))

table(sapply(strsplit(genome_data$genome, "_"), function(x) x[1]))
# all BATS, BGEO, HOTS, MALA, TARA less than 50m water depth from the water column
# all GORG

check_mard <- genome_data[sapply(strsplit(genome_data$genome, "_"), function(x) x[1]) == "MARD", ]
check_mard$mar_id <- sapply(strsplit(check_mard$genome, "_"), function(x) x[4])
sum(check_mard$mar_id %in% marref$mar_id)
sum(check_mard$mar_id %in% mardb$mar_id)
dim(check_mard[!check_mard$mar_id %in% c(marref$mar_id, mardb$mar_id), ])
# not all OMDv1 MarDB entries are actually found in their metadata...
# for now: ignore the 98 missing entries
marref[marref$mar_id %in% check_mard$mar_id, c("id", "mar_id", "isol_env_package", "isol_country", "isol_location", "isol_lat_lon", "isol_depth", "isol_env_biome", "isol_env_feature", "isol_env_material")] %>% 
  filter(isol_env_package == "Water") %>% 
  filter(isol_depth != "") %>% 
  filter(isol_lat_lon != "")
mardb[mardb$mar_id %in% check_mard$mar_id, c("id", "mar_id", "isol_env_package", "isol_country", "isol_location", "isol_lat_lon", "isol_depth", "isol_env_biome", "isol_env_feature", "isol_env_material")] %>% 
  filter(isol_env_package == "Water") %>% 
  filter(!is.na(isol_depth) & isol_depth <= 50) %>% 
  filter(isol_lat_lon != "")
# not worth to pursue further, I think
# skip MARDB --> inconsistent and incomplete metadata

genome_data_filt <- genome_data %>% 
  filter(
    detected_in_the_water_column == "true", 
    grepl("d__Bacteria;", gtdb_taxonomy),
    (depth <= 50 & !is.na(depth)) | sapply(strsplit(genome, "_"), function(x) x[1]) == "GORG",
    (abs(latitude) <= 45 & !is.na(latitude)) | sapply(strsplit(genome, "_"), function(x) x[1]) == "GORG"
  )
# 13230 genomes remaining

maps::map(
  "world", # world coastlines
  resolution = 2, # highest resolution
  # xlim = c(0, 40), # area to be plotted
  # ylim = c(50, 70), 
  fill = T, # fill land masses
  col = "grey90", # land color
  border = NA,
  mar = c(0, 0, 0, 0)
)
points(
  genome_data_filt$longitude,
  genome_data_filt$latitude,
  pch = 16
)
abline(h = c(-45, 45))
# GORG not shown because no coordinates, but according to paper all tropics

write.table(genome_data_filt, "genome_data_filt.txt", sep = "\t", quote = F, row.names = F)
# cut -f1 genome_data_filt.txt | sed '1d' | parallel -j20 'mv genomes-fasta/{}.fa genomes_filt/'
# conda activate gtdbtk-2.3.0
# gtdbtk classify_wf --genome_dir genomes_filt --out_dir gtdbtk_out -x fa --min_af 0.5 --cpus 72 --mash_db /bio/Databases/GTDB/R214/gtdbtk-2.3.0/release214/mash

genome_data_gtdb <- read.table("gtdbtk.bac120.summary.tsv", h = T, sep = "\t")
genome_data_gtdb_filt <- genome_data_gtdb %>%
  # filter(grepl("g__Brachybacterium|g__Yoonia|g__Qipengyuania|g__Micrococcus", classification))
  filter(grepl("f__Dermabacteraceae|f__Rhodobacteraceae|f__Sphingomonadaceae|f__Micrococcaceae", classification))
table(sort(sapply(strsplit(genome_data_gtdb_filt$classification, ";"), function(x) x[[5]])))
# only f__Rhodobacteraceae and f__Sphingomonadaceae represented