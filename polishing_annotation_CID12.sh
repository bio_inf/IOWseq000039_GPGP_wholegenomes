### IOWseq000039 strain analysis: CID12

# polishing assemblies with pilon
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Assembly_polishing
# only works on phy servers (but not phy-4)
mkdir CID12
cd CID12
# copy assembly
# warning: adjust input path once /bio/projects has been cleaned-up
cp /bio/projects/2021/GPGP_wholegenomes/1106_SID07_CID12.mp.fasta assembly.fasta
# extract Illumina reads
mkdir illumina_reads
cd illumina_reads
ln -s /bio/Raw_data/IOWseq000039_GPGP_wholegenomes/Seq_data/Illumina_reads/MiSeq_664_1452_Pacific_Plastic/1452_001_SID07_CID12/001_TSLF_SID07_CID12_S60_L001_R1_001.fastq.gz CID12_R1.fastq.gz 
ln -s /bio/Raw_data/IOWseq000039_GPGP_wholegenomes/Seq_data/Illumina_reads/MiSeq_664_1452_Pacific_Plastic/1452_001_SID07_CID12/001_TSLF_SID07_CID12_S60_L001_R2_001.fastq.gz CID12_R2.fastq.gz 
# quality control of Illumina reads
conda activate qc-env
fastqc CID12_R* -t 2 -o ./
# run phiX removal, right-trim adapters, do quality trimming
cd ..
mkdir illumina_qc
conda activate bbmap-39.01
# phiX removal
bbduk.sh in=illumina_reads/CID12_R1.fastq.gz in2=illumina_reads/CID12_R2.fastq.gz out=illumina_qc/trim1_R1.fastq.gz out2=illumina_qc/trim1_R2.fastq.gz ref=phix k=28 stats=illumina_qc/stats1.txt threads=32 > illumina_qc/trim1.log 2>&1
# to remove the library adaptor (right): removes adapters on 3' and of sequence and trailing bases
bbduk.sh in=illumina_qc/trim1_R1.fastq.gz in2=illumina_qc/trim1_R2.fastq.gz out=illumina_qc/trim2_R1.fastq.gz out2=illumina_qc/trim2_R2.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=illumina_qc/stats2.txt threads=32 > illumina_qc/trim2.log 2>&1
# quality trimming
bbduk.sh in=illumina_qc/trim2_R1.fastq.gz in2=illumina_qc/trim2_R2.fastq.gz out=illumina_qc/trim3_R1.fastq.gz out2=illumina_qc/trim3_R2.fastq.gz qtrim=w,4 trimq=20 minlength=100 trimpolyg=10 threads=32 > illumina_qc/trim3.log 2>&1
# repeat fastqc
conda activate qc-env
cd illumina_qc
fastqc trim3_*.fastq.gz -t 2 -o ./
cd ..
# map reads
conda activate coverm-0.6.1
bwa index assembly.fasta
bwa mem -t 32 assembly.fasta illumina_qc/trim3_R1.fastq.gz illumina_qc/trim3_R2.fastq.gz | samtools view -b - | samtools sort -@ 32 - > mapped.bam
samtools index -@ 32 mapped.bam
conda deactivate
# run pilon
conda activate pilon-1.24
pilon -Xmx512G --genome assembly.fasta --bam mapped.bam --output polished_assembly > pilon.log
conda deactivate
# rename assembly to file ending fa
mv polished_assembly.fasta polished_assembly.fa
# assembly stats: 1 contig (3,969,927bp)

# assembly quality
module load metawrap/1.3.2
conda activate metawrap-env
mkdir -p tmp
checkm lineage_wf -x fa ./ checkm_out -t 32 --tmpdir tmp --pplacer_threads 2
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#  Bin Id                     Marker lineage          # genomes   # markers   # marker sets   0    1    2   3   4   5+   Completeness   Contamination   Strain heterogeneity
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#  polished_assembly   o__Actinomycetales (UID1572)      580         286           171        5   281   0   0   0   0       97.66            0.00               0.00        
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
conda activate checkm2-0.1.3
checkm2 predict --threads 32 -x fa --allmodels --input ./ --output-directory checkm2_out
#Name    Completeness_General    Contamination   Completeness_Specific   Completeness_Model_Used Translation_Table_Used  Additional_Notes
#polished_assembly       98.61   0.66    100.0   Neural Network (Specific Model) 11      None

conda activate busco-5.4.4
busco -i polished_assembly.fa -o busco_out -m genome --auto-lineage-prok -c 32 --download_path /bio/Databases/BUSCO/20210628/busco
#        --------------------------------------------------
#        |Results from generic domain bacteria_odb10       |
#        --------------------------------------------------
#        |C:97.6%[S:97.6%,D:0.0%],F:1.6%,M:0.8%,n:124      |
#        |121    Complete BUSCOs (C)                       |
#        |121    Complete and single-copy BUSCOs (S)       |
#        |0      Complete and duplicated BUSCOs (D)        |
#        |2      Fragmented BUSCOs (F)                     |
#        |1      Missing BUSCOs (M)                        |
#        |124    Total BUSCO groups searched               |
#        --------------------------------------------------
#        WARNING: wrong lineage detected in auto mode
#        --------------------------------------------------
#        |Results from dataset micrococcales_odb10         |
#        --------------------------------------------------
#        |C:95.9%[S:95.7%,D:0.2%],F:2.2%,M:1.9%,n:537      |
#        |515    Complete BUSCOs (C)                       |
#        |514    Complete and single-copy BUSCOs (S)       |
#        |1      Complete and duplicated BUSCOs (D)        |
#        |12     Fragmented BUSCOs (F)                     |
#        |10     Missing BUSCOs (M)                        |
#        |537    Total BUSCO groups searched               |
#        --------------------------------------------------
busco -i polished_assembly.fa -o busco_out_actinobacteria -m genome -l actinobacteria_phylum_odb10 -c 32 --offline --download_path /bio/Databases/BUSCO/20210628/busco
#        --------------------------------------------------
#        |Results from dataset actinobacteria_phylum_odb10 |
#        --------------------------------------------------
#        |C:96.2%[S:96.2%,D:0.0%],F:2.1%,M:1.7%,n:292      |
#        |281    Complete BUSCOs (C)                       |
#        |281    Complete and single-copy BUSCOs (S)       |
#        |0      Complete and duplicated BUSCOs (D)        |
#        |6      Fragmented BUSCOs (F)                     |
#        |5      Missing BUSCOs (M)                        |
#        |292    Total BUSCO groups searched               |
#        --------------------------------------------------

# Taxonomic classification based on marker genes
conda activate gtdbtk-2.1.0
gtdbtk classify_wf --genome_dir ./ --out_dir gtdbtk_out -x fa --cpus 32 --pplacer_cpus 2 --min_af 0.5

# repeated with GTDB214
conda activate gtdbtk-2.3.0
gtdbtk classify_wf --genome_dir ./ --out_dir gtdbtk214_out -x fa --min_af 0.5 --cpus 32 --mash_db /bio/Databases/GTDB/R214/gtdbtk-2.3.0/release214/mash

# Gene prediction and initial annotation with PROKKA
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation
mkdir CID12
cd CID12
conda activate prokka-1.14.6
prokka --kingdom Bacteria --outdir prokka_out --prefix CID12 --cpus 8 --addgenes --centre IOW --compliant ../../Assembly_polishing/CID12/polished_assembly.fa >> CID12_prokka.log 2>&1
# parse annotation output for CDS only
cd prokka_out
head -1 CID12.tsv > prokka_cds_annotation.txt
grep -w "CDS" CID12.tsv >> prokka_cds_annotation.txt

# KEGG annotation
mkdir kegg_out
conda activate checkm2-0.1.3 
diamond blastp -d /bio/Databases/KEGG/release_20230117/diamond/kegg_genes.dmnd --sensitive -e 1e-5 -q prokka_out/CID12.faa --top 10 -p 50 -o kegg_out/out_kegg_diamond.txt -f 6 -b 8
diamond makedb --in prokka_out/CID12.faa -d kegg_out/cds_db
diamond blastp -d kegg_out/cds_db.dmnd -q prokka_out/CID12.faa -k 1 -o kegg_out/out_self_diamond.txt -f 6
rm kegg_out/cds_db.dmnd
# append blast ratio score to kegg output and filter to 40%
conda activate r-4.2.2
/bio/Common_repositories/workflow_templates/metaG_Illumina_PE/scripts/parse_kegg.R -b kegg_out/out_kegg_diamond.txt -s kegg_out/out_self_diamond.txt -m /bio/Databases/KEGG/release_20230117/mapping_info -c 0.4 -t 2 -o kegg_out/out_kegg_parsed

# KOfamscan
# maybe more sensitive than KEGG blast, but also maybe more susceptible to false positives...
mkdir kofam_out
conda activate kofamscan-1.3.0
exec_annotation -o kofam_out/out_kofamscan.txt -p /bio/Databases/KOfam/Dec2022/kofamscan/profiles -k /bio/Databases/KOfam/Dec2022/kofamscan/ko_list --tmp-dir=tmp --cpu 12 -E 0.01 prokka_out/CID12.faa
echo -e "gene_name\tKO_number\tthreshold\tscore\te_value\tKO_definition" > kofam_out/out_kofam_parsed.txt
grep "^\*" kofam_out/out_kofamscan.txt | sed 's/^\* //' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' >> kofam_out/out_kofam_parsed.txt
rm -rf tmp

# Comment Thomas:
# in Kegg mal schauen, ob der prophyrin-synthese pathway so detailliert ist, dass man heme-synthesis von dem ganzen anderen Kram trennen kann? damit hätte man ja auch die phycobiline der Cyans abgedeckt

# RemeDB
mkdir remedb_out
conda activate dbcan-3.0.7
hmmscan -E 0.01 --cpu 12 --tblout remedb_out/all_out_table.txt --pfamtblout remedb_out/all_out_full.txt /bio/Databases/RemeDB/release_2019/RemeDB_Linux/database/hmm/combined.hmm prokka_out/CID12.faa > remedb_out/all_out_hmm.txt
hmmscan -E 0.01 --cpu 12 --tblout remedb_out/hydrocarbon_table.txt /bio/Databases/RemeDB/release_2019/RemeDB_Linux/database/hmm/hydrocarbon_hmm/hydrocarbon.hmm prokka_out/CID12.faa > remedb_out/hydrocarbon_hmm.txt
hmmscan -E 0.01 --cpu 12 --tblout remedb_out/dye_table.txt /bio/Databases/RemeDB/release_2019/RemeDB_Linux/database/hmm/dye_hmm/dye.hmm prokka_out/CID12.faa > remedb_out/dye_hmm.txt
hmmscan -E 0.01 --cpu 12 --tblout remedb_out/plastic_table.txt /bio/Databases/RemeDB/release_2019/RemeDB_Linux/database/hmm/plastic_hmm/plastic.hmm prokka_out/CID12.faa > remedb_out/plastic_hmm.txt
# convert to tab-separated format
# warning: I did not further filter the output based on score
# I would be very skeptical about hits with score <50...
grep -v "^\#" remedb_out/all_out_table.txt | sed -E 's/ +/\t/g' | awk '$6 >= 50' | sed '1i target_name\ttarget_accession\tquery_name\tquery_accession\tevalue_full\tscore_full\tbias_full\tevalue_domain\tscore_domain\tbias_domain\texp\treg\tclu\tov\tenv\tdom\trep\tinc\ttarget_description' > remedb_out/all_out_table_filt.txt
grep -v "^\#" remedb_out/hydrocarbon_table.txt | sed -E 's/ +/\t/g' | awk '$6 >= 50' | sed '1i target_name\ttarget_accession\tquery_name\tquery_accession\tevalue_full\tscore_full\tbias_full\tevalue_domain\tscore_domain\tbias_domain\texp\treg\tclu\tov\tenv\tdom\trep\tinc\ttarget_description' > remedb_out/hydrocarbon_table_filt.txt
grep -v "^\#" remedb_out/dye_table.txt | sed -E 's/ +/\t/g' | awk '$6 >= 50' | sed '1i target_name\ttarget_accession\tquery_name\tquery_accession\tevalue_full\tscore_full\tbias_full\tevalue_domain\tscore_domain\tbias_domain\texp\treg\tclu\tov\tenv\tdom\trep\tinc\ttarget_description' > remedb_out/dye_table_filt.txt
grep -v "^\#" remedb_out/plastic_table.txt | sed -E 's/ +/\t/g' | awk '$6 >= 50' | sed '1i target_name\ttarget_accession\tquery_name\tquery_accession\tevalue_full\tscore_full\tbias_full\tevalue_domain\tscore_domain\tbias_domain\texp\treg\tclu\tov\tenv\tdom\trep\tinc\ttarget_description' > remedb_out/plastic_table_filt.txt

# PlasticDB
mkdir plasticdb_out
conda activate checkm2-0.1.3 
diamond blastp -d /bio/Databases/PlasticDB/release_2022/diamond/plasticdb.dmnd --sensitive -e 1e-5 -q prokka_out/CID12.faa --top 10 -p 10 -o plasticdb_out/out_plasticdb_diamond.txt -f 6 -b 8
# parse with blast score ratio
conda activate r-4.2.2
../parse_plasticdb.R -b plasticdb_out/out_plasticdb_diamond.txt -s kegg_out/out_self_diamond.txt -c 0.4 -o plasticdb_out/out_plasticdb_parsed

# /bio/Schott/Hamburg_PET (maybe later)?

# Identify membrane-bound proteins: https://dtu.biolib.com/DeepTMHMM
conda activate seqkit-2.3.1
seqkit split2 -p 10 -O faa_split -f -1 prokka_out/CID12.faa
# cannot be run locally (at least not easily)
# needs to be submitted in batches to webserver
# manually unzip and concatenate 3line and gff output
cat ./*/predicted_topologies.3line > CID12_predicted_topologies.3line
cat ./*/TMRs.gff3 | sed '/\#\#gff-version 3/d' > CID12_TMRs.gff3
# collect information on number of transmembrane helices
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID12/deeptmhmm_out
grep "Number of predicted TMRs:" CID12_TMRs.gff3 | sed -e 's/\# //' -e 's/ Number of predicted TMRs: /\t/' > tmp1
grep '^>' CID12_predicted_topologies.3line | sed -e 's/^>//' -e 's/ | /\t/' > tmp2
# check order and combine
diff <(cut -f1 tmp1) <(cut -f1 tmp2)
echo -e "gene_name\ttm_regions\tpredicted_type" > CID12_deeptmhmm_summary.txt
paste tmp1 <(cut -f2 tmp2) >> CID12_deeptmhmm_summary.txt
rm tmp*

# InterProScan
module load interproscan/5.60-92.0
conda activate base
mkdir interpro_out
cd interpro_out
interproscan.sh -i ../prokka_out/CID12.faa -b CID12_iprout -goterms -cpu 40 -T tmp -f TSV >> ipr.log 2>&1
sed -i '1i feature_id\tseq_md5\tseq_len\tanalysis\tsignature_accession\tsignature_description\tstart\tend\tscore\tstatus\tdate_run\tipr_accession\tipr_description\tgo_terms' CID12_iprout.tsv
rm -rf tmp

# AntiSMASH
conda activate antismash-6.1.1
antismash -c 40 --taxon bacteria --cb-general --cb-knownclusters --cb-subclusters --asf --output-dir antismash_out prokka_out/CID12.gbk
# inspect output in browser (copy to local computer)

# BGCfinder: does not work and no help/tutorial available --> abandoned

# blastp against NCBI nr
mkdir nr_out
conda activate checkm2-0.1.3 
diamond blastp -d /bio/Databases/NCBI_nr/20221210/diamond/diamond_nr.dmnd --very-sensitive -e 1e-5 -q prokka_out/CID12.faa --top 10 -p 90 -o nr_out/out_nr_diamond.txt -f 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen stitle staxids sscinames salltitles -b 8
# parse with blast score ratio
conda activate r-4.2.2
../parse_nr.R -b nr_out/out_nr_diamond.txt -s kegg_out/out_self_diamond.txt -c 0.4 -o nr_out/out_nr_parsed

# Final output files of annotation:
ls -1 \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Assembly_polishing/CID12/gtdbtk_out/gtdbtk.bac120.summary.tsv \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID12/prokka_out/prokka_cds_annotation.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID12/kegg_out/out_kegg_parsed_best.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID12/kofam_out/out_kofam_parsed.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID12/deeptmhmm_out/CID12_deeptmhmm_summary.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID12/remedb_out/all_out_table_filt.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID12/plasticdb_out/out_plasticdb_parsed_best.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID12/interpro_out/CID12_iprout.tsv \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID12/nr_out/out_nr_parsed_best.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID12/antismash_out/index.html
# additional output is available in the respective output directories

# extract ribosomal sequences
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID12/prokka_out
grep "barrnap" CID12.gff | cut -f9 | cut -d';' -f1 | sed 's/ID=//' > tmp_rRNA.accnos
conda activate bbmap-39.01
filterbyname.sh in=CID12.ffn out=../CID12_rRNA.ffn names=tmp_rRNA.accnos include=t ow=t

# Move final data products to dedicated directory for long term archiving
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/
cp Assembly_polishing/CID12/polished_assembly.fa ../Final_results/Assemblies/CID12_polished_assembly.fa
cp -r Strain_annotation/CID12/prokka_out ../Final_results/Gene_prediction/CID12_prokka
rm ../Final_results/Gene_prediction/CID12_prokka/prokka_cds_annotation.txt ../Final_results/Gene_prediction/CID12_prokka/tmp_rRNA.accnos
cp Strain_annotation/CID12/kegg_out/out_kegg_parsed_best.txt ../Final_results/KEGG_annotation/CID12_kegg_parsed_best.txt
cp Strain_annotation/CID12/kofam_out/out_kofam_parsed.txt ../Final_results/KOfam_annotation/CID12_kofam_parsed.txt
cp Strain_annotation/CID12/deeptmhmm_out/CID12_deeptmhmm_summary.txt ../Final_results/TMHMM_annotation/CID12_deeptmhmm_summary.txt
cp Strain_annotation/CID12/interpro_out/CID12_iprout.tsv ../Final_results/IPR_annotation/CID12_iprout.tsv
cp Strain_annotation/CID12/nr_out/out_nr_parsed_best.txt ../Final_results/NR_annotation/CID12_nr_parsed_best.txt
cp -r Strain_annotation/CID12/antismash_out ../Final_results/BGC_annotation/CID12_antismash

