### IOWseq000039 strain analysis: CID17

# polishing assemblies with pilon
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Assembly_polishing
# only works on phy servers (but not phy-4)
mkdir CID17
cd CID17
# copy assembly
# warning: adjust input path once /bio/projects has been cleaned-up
cp /bio/projects/2021/GPGP_wholegenomes/1106_SID11_CID17.mp.fasta assembly.fasta
# extract Illumina reads
mkdir illumina_reads
tar xvf /bio/projects/2021/GPGP_wholegenomes/1106_1310_ReSeq_Reprep_Pacific_Plastic_Illumina_raw/#1106_#1310_RePrep_ReSeq_Pacific_Plastic_MiSeq_NextSeq.tar -C illumina_reads
# CID17 only sequences with Illumina
# quality control of Illumina reads
cd illumina_reads/MiSeq_606_#1106_RePrep_Pacific_Plastic_CID_6_9_10_21_22_SID_11_15
conda activate qc-env
fastqc 1106_SID11_CID17_S41_L001_R* -t 2 -o ./
# run phiX removal, right-trim adapters, do quality trimming
cd ../..
mkdir illumina_qc
conda activate bbmap-39.01
# phiX removal
bbduk.sh in=illumina_reads/MiSeq_606_#1106_RePrep_Pacific_Plastic_CID_6_9_10_21_22_SID_11_15/1106_SID11_CID17_S41_L001_R1_001.fastq.gz in2=illumina_reads/MiSeq_606_#1106_RePrep_Pacific_Plastic_CID_6_9_10_21_22_SID_11_15/1106_SID11_CID17_S41_L001_R2_001.fastq.gz out=illumina_qc/trim1_R1.fastq.gz out2=illumina_qc/trim1_R2.fastq.gz ref=phix k=28 stats=illumina_qc/stats1.txt threads=32 fastawrap=300 > illumina_qc/trim1.log 2>&1
# to remove the library adaptor (right): removes adapters on 3' and of sequence and trailing bases
bbduk.sh in=illumina_qc/trim1_R1.fastq.gz in2=illumina_qc/trim1_R2.fastq.gz out=illumina_qc/trim2_R1.fastq.gz out2=illumina_qc/trim2_R2.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=illumina_qc/stats2.txt threads=32 fastawrap=300 > illumina_qc/trim2.log 2>&1
# quality trimming
bbduk.sh in=illumina_qc/trim2_R1.fastq.gz in2=illumina_qc/trim2_R2.fastq.gz out=illumina_qc/trim3_R1.fastq.gz out2=illumina_qc/trim3_R2.fastq.gz qtrim=w,4 trimq=20 minlength=100 trimpolyg=10 threads=32 fastawrap=300 > illumina_qc/trim3.log 2>&1
# repeat fastqc
conda activate qc-env
cd illumina_qc
fastqc trim3_*.fastq.gz -t 2 -o ./
cd ..
# map reads
conda activate coverm-0.6.1
bwa index assembly.fasta
bwa mem -t 32 assembly.fasta illumina_qc/trim3_R1.fastq.gz illumina_qc/trim3_R2.fastq.gz | samtools view -b - | samtools sort -@ 32 - > mapped.bam
samtools index -@ 32 mapped.bam
conda deactivate
# run pilon
conda activate pilon-1.24
pilon -Xmx512G --genome assembly.fasta --bam mapped.bam --output polished_assembly > pilon.log
conda deactivate
# rename assembly to file ending fa
mv polished_assembly.fasta polished_assembly.fa
# assembly stats
conda activate bbmap-39.01
stats.sh in=polished_assembly.fa
#Minimum         Number          Number          Total           Total           Scaffold
#Scaffold        of              of              Scaffold        Contig          Contig
#Length          Scaffolds       Contigs         Length          Length          Coverage
#--------        --------------  --------------  --------------  --------------  --------
#    All                      5               5       4,212,794       4,212,794   100.00%
#  25 KB                      5               5       4,212,794       4,212,794   100.00%
#  50 KB                      4               4       4,175,553       4,175,553   100.00%
# 100 KB                      3               3       4,113,762       4,113,762   100.00%
# 250 KB                      1               1       3,838,863       3,838,863   100.00%
# 500 KB                      1               1       3,838,863       3,838,863   100.00%
#   1 MB                      1               1       3,838,863       3,838,863   100.00%
# 2.5 MB                      1               1       3,838,863       3,838,863   100.00%

# assembly quality
module load metawrap/1.3.2
conda activate metawrap-env
mkdir -p tmp
checkm lineage_wf -x fa ./ checkm_out -t 32 --tmpdir tmp --pplacer_threads 2
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#  Bin Id                      Marker lineage          # genomes   # markers   # marker sets   0    1    2   3   4   5+   Completeness   Contamination   Strain heterogeneity
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#  polished_assembly   f__Rhodobacteraceae (UID3356)       67         615           329        1   606   8   0   0   0       99.70            1.02               0.00
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

conda activate checkm2-0.1.3
checkm2 predict --threads 32 -x fa --allmodels --input ./ --output-directory checkm2_out
#Name    Completeness_General    Contamination   Completeness_Specific   Completeness_Model_Used Translation_Table_Used  Additional_Notes
#polished_assembly       100.0   0.45    99.97   Neural Network (Specific Model) 11      None

conda activate busco-5.4.4
busco -i polished_assembly.fa -o busco_out -m genome --auto-lineage-prok -c 32 --download_path /bio/Databases/BUSCO/20210628/busco
#        --------------------------------------------------
#        |Results from generic domain bacteria_odb10       |
#        --------------------------------------------------
#        |C:98.4%[S:97.6%,D:0.8%],F:1.6%,M:0.0%,n:124      |
#        |122    Complete BUSCOs (C)                       |
#        |121    Complete and single-copy BUSCOs (S)       |
#        |1      Complete and duplicated BUSCOs (D)        |
#        |2      Fragmented BUSCOs (F)                     |
#        |0      Missing BUSCOs (M)                        |
#        |124    Total BUSCO groups searched               |
#        --------------------------------------------------
#        --------------------------------------------------
#        |Results from dataset rhodobacterales_odb10       |
#        --------------------------------------------------
#        |C:99.7%[S:99.6%,D:0.1%],F:0.0%,M:0.3%,n:833      |
#        |831    Complete BUSCOs (C)                       |
#        |830    Complete and single-copy BUSCOs (S)       |
#        |1      Complete and duplicated BUSCOs (D)        |
#        |0      Fragmented BUSCOs (F)                     |
#        |2      Missing BUSCOs (M)                        |
#        |833    Total BUSCO groups searched               |
#        --------------------------------------------------

# Taxonomic classification based on marker genes
conda activate gtdbtk-2.1.0
gtdbtk classify_wf --genome_dir ./ --out_dir gtdbtk_out -x fa --cpus 32 --pplacer_cpus 2 --min_af 0.5

# repeated with GTDB214
conda activate gtdbtk-2.3.0
gtdbtk classify_wf --genome_dir ./ --out_dir gtdbtk214_out -x fa --min_af 0.5 --cpus 32 --mash_db /bio/Databases/GTDB/R214/gtdbtk-2.3.0/release214/mash

# just for fun, try reassembly with spades (test trusted contigs options)
# I am using the assembly prior to polishing here, since if reassembly works there would be another polishing step afterwards anyway
# mkdir spades_reassembly
# cd spades_reassembly
# module load metawrap/1.3.2
# conda activate metawrap-env
# samtools view -F 256 ../mapped.bam | /sw/bio/metaWRAP/1.3.2/metaWRAP/bin/metawrap-scripts/filter_reads_for_bin_reassembly.py ../ ./ 2 5
# spades.py -t 64 -m 150 --tmp tmp_strict --careful --trusted-contigs ../assembly.fasta -1 assembly.strict_1.fastq -2 assembly.strict_2.fastq -o reassembly_strict
# /sw/bio/metaWRAP/1.3.2/metaWRAP/bin/metawrap-scripts/rm_short_contigs.py 1000 reassembly_strict/scaffolds.fasta > reassembly_strict/scaffolds_1000.fasta
# does not work: insert too short

# Gene prediction and initial annotation with PROKKA
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation
mkdir CID17
cd CID17
conda activate prokka-1.14.6
prokka --kingdom Bacteria --outdir prokka_out --prefix CID17 --cpus 8 --addgenes --centre IOW --compliant ../../Assembly_polishing/CID17/polished_assembly.fasta >> CID17_prokka.log 2>&1
# parse annotation output for CDS only
cd prokka_out
head -1 CID17.tsv > prokka_cds_annotation.txt
grep -w "CDS" CID17.tsv >> prokka_cds_annotation.txt

# KEGG annotation
mkdir kegg_out
conda activate checkm2-0.1.3 
diamond blastp -d /bio/Databases/KEGG/release_20230117/diamond/kegg_genes.dmnd --sensitive -e 1e-5 -q prokka_out/CID17.faa --top 10 -p 50 -o kegg_out/out_kegg_diamond.txt -f 6 -b 8
diamond makedb --in prokka_out/CID17.faa -d kegg_out/cds_db
diamond blastp -d kegg_out/cds_db.dmnd -q prokka_out/CID17.faa -k 1 -o kegg_out/out_self_diamond.txt -f 6
rm kegg_out/cds_db.dmnd
# append blast ratio score to kegg output and filter to 40%
conda activate r-4.2.2
/bio/Common_repositories/workflow_templates/metaG_Illumina_PE/scripts/parse_kegg.R -b kegg_out/out_kegg_diamond.txt -s kegg_out/out_self_diamond.txt -m /bio/Databases/KEGG/release_20230117/mapping_info -c 0.4 -t 2 -o kegg_out/out_kegg_parsed

# KOfamscan
# maybe more sensitive than KEGG blast, but also maybe more susceptible to false positives...
mkdir kofam_out
conda activate kofamscan-1.3.0
exec_annotation -o kofam_out/out_kofamscan.txt -p /bio/Databases/KOfam/Dec2022/kofamscan/profiles -k /bio/Databases/KOfam/Dec2022/kofamscan/ko_list --tmp-dir=tmp --cpu 12 -E 0.01 prokka_out/CID17.faa
echo -e "gene_name\tKO_number\tthreshold\tscore\te_value\tKO_definition" > kofam_out/out_kofam_parsed.txt
grep "^\*" kofam_out/out_kofamscan.txt | sed 's/^\* //' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' >> kofam_out/out_kofam_parsed.txt
rm -rf tmp

# MicRhoDE
# mkdir micrhode_out
# conda activate checkm2-0.1.3 
# diamond blastp -d /bio/Databases/MicRhoDE/release_2015/diamond/micrhode_db.dmnd --sensitive -e 1e-5 -q prokka_out/CID17.faa --top 10 -p 24 -o micrhode_out/out_micrhode_diamond.txt -f 6
# 1 gene found! --> but much better KEGG hit (to different function) available
# MicRhoDE hit most likely a false positive

# Comment Thomas:
# in Kegg mal schauen, ob der prophyrin-synthese pathway so detailliert ist, dass man heme-synthesis von dem ganzen anderen Kram trennen kann? damit hätte man ja auch die phycobiline der Cyans abgedeckt

# RemeDB
mkdir remedb_out
conda activate dbcan-3.0.7
hmmscan -E 0.01 --cpu 12 --tblout remedb_out/all_out_table.txt --pfamtblout remedb_out/all_out_full.txt /bio/Databases/RemeDB/release_2019/RemeDB_Linux/database/hmm/combined.hmm prokka_out/CID17.faa > remedb_out/all_out_hmm.txt
hmmscan -E 0.01 --cpu 12 --tblout remedb_out/hydrocarbon_table.txt /bio/Databases/RemeDB/release_2019/RemeDB_Linux/database/hmm/hydrocarbon_hmm/hydrocarbon.hmm prokka_out/CID17.faa > remedb_out/hydrocarbon_hmm.txt
hmmscan -E 0.01 --cpu 12 --tblout remedb_out/dye_table.txt /bio/Databases/RemeDB/release_2019/RemeDB_Linux/database/hmm/dye_hmm/dye.hmm prokka_out/CID17.faa > remedb_out/dye_hmm.txt
hmmscan -E 0.01 --cpu 12 --tblout remedb_out/plastic_table.txt /bio/Databases/RemeDB/release_2019/RemeDB_Linux/database/hmm/plastic_hmm/plastic.hmm prokka_out/CID17.faa > remedb_out/plastic_hmm.txt
# convert to tab-separated format
# warning: I did not further filter the output based on score
# I would be very skeptical about hits with score <50...
grep -v "^\#" remedb_out/all_out_table.txt | sed -E 's/ +/\t/g' | awk '$6 >= 50' | sed '1i target_name\ttarget_accession\tquery_name\tquery_accession\tevalue_full\tscore_full\tbias_full\tevalue_domain\tscore_domain\tbias_domain\texp\treg\tclu\tov\tenv\tdom\trep\tinc\ttarget_description' > remedb_out/all_out_table_filt.txt
grep -v "^\#" remedb_out/hydrocarbon_table.txt | sed -E 's/ +/\t/g' | awk '$6 >= 50' | sed '1i target_name\ttarget_accession\tquery_name\tquery_accession\tevalue_full\tscore_full\tbias_full\tevalue_domain\tscore_domain\tbias_domain\texp\treg\tclu\tov\tenv\tdom\trep\tinc\ttarget_description' > remedb_out/hydrocarbon_table_filt.txt
grep -v "^\#" remedb_out/dye_table.txt | sed -E 's/ +/\t/g' | awk '$6 >= 50' | sed '1i target_name\ttarget_accession\tquery_name\tquery_accession\tevalue_full\tscore_full\tbias_full\tevalue_domain\tscore_domain\tbias_domain\texp\treg\tclu\tov\tenv\tdom\trep\tinc\ttarget_description' > remedb_out/dye_table_filt.txt
grep -v "^\#" remedb_out/plastic_table.txt | sed -E 's/ +/\t/g' | awk '$6 >= 50' | sed '1i target_name\ttarget_accession\tquery_name\tquery_accession\tevalue_full\tscore_full\tbias_full\tevalue_domain\tscore_domain\tbias_domain\texp\treg\tclu\tov\tenv\tdom\trep\tinc\ttarget_description' > remedb_out/plastic_table_filt.txt

# PlasticDB
mkdir plasticdb_out
conda activate checkm2-0.1.3 
diamond blastp -d /bio/Databases/PlasticDB/release_2022/diamond/plasticdb.dmnd --sensitive -e 1e-5 -q prokka_out/CID17.faa --top 10 -p 50 -o plasticdb_out/out_plasticdb_diamond.txt -f 6 -b 8
# parse with blast score ratio
conda activate r-4.2.2
../parse_plasticdb.R -b plasticdb_out/out_plasticdb_diamond.txt -s kegg_out/out_self_diamond.txt -c 0.4 -o plasticdb_out/out_plasticdb_parsed

# /bio/Schott/Hamburg_PET (maybe later)?

# Identify membrane-bound proteins: https://dtu.biolib.com/DeepTMHMM
conda activate seqkit-2.3.1
seqkit split2 -p 10 -O faa_split -f -1 prokka_out/CID17.faa
# cannot be run locally (at least not easily)
# needs to be submitted in batches to webserver
# manually unzip and concatenate 3line and gff output
cat ./*/predicted_topologies.3line > CID17_predicted_topologies.3line
cat ./*/TMRs.gff3 | sed '/\#\#gff-version 3/d' > CID17_TMRs.gff3
# collecr information on number of transmembrane helices
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID17/deeptmhmm_out
grep "Number of predicted TMRs:" CID17_TMRs.gff3 | sed -e 's/\# //' -e 's/ Number of predicted TMRs: /\t/' > tmp1
grep '^>' CID17_predicted_topologies.3line | sed -e 's/^>//' -e 's/ | /\t/' > tmp2
# check order and combine
diff <(cut -f1 tmp1) <(cut -f1 tmp2)
echo -e "gene_name\ttm_regions\tpredicted_type" > CID17_deeptmhmm_summary.txt
paste tmp1 <(cut -f2 tmp2) >> CID17_deeptmhmm_summary.txt
rm tmp*

# InterProScan
module load interproscan/5.60-92.0
mkdir interpro_out
cd interpro_out
interproscan.sh -i ../prokka_out/CID17.faa -b CID17_iprout -goterms -cpu 40 -T tmp -f TSV >> ipr.log 2>&1
sed -i '1i feature_id\tseq_md5\tseq_len\tanalysis\tsignature_accession\tsignature_description\tstart\tend\tscore\tstatus\tdate_run\tipr_accession\tipr_description\tgo_terms' CID17_iprout.tsv
rm -rf tmp

# AntiSMASH
conda activate antismash-6.1.1
antismash -c 40 --taxon bacteria --cb-general --cb-knownclusters --cb-subclusters --asf --output-dir antismash_out prokka_out/CID17.gbk
# inspect output in browser (copy to local computer)

# BGCfinder: does not work and no help/tutorial available --> abandoned

# blastp against NCBI nr
mkdir nr_out
conda activate checkm2-0.1.3 
diamond blastp -d /bio/Databases/NCBI_nr/20221210/diamond/diamond_nr.dmnd --very-sensitive -e 1e-5 -q prokka_out/CID17.faa --top 10 -p 50 -o nr_out/out_nr_diamond.txt -f 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen stitle staxids sscinames salltitles -b 8
# parse with blast score ratio
conda activate r-4.2.2
../parse_nr.R -b nr_out/out_nr_diamond.txt -s kegg_out/out_self_diamond.txt -c 0.4 -o nr_out/out_nr_parsed

# Final output files of annotation:
ls -1 \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Assembly_polishing/CID17/gtdbtk_out/gtdbtk.bac120.summary.tsv \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID17/prokka_out/prokka_cds_annotation.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID17/kegg_out/out_kegg_parsed_best.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID17/kofam_out/out_kofam_parsed.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID17/deeptmhmm_out/CID17_deeptmhmm_summary.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID17/remedb_out/all_out_table_filt.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID17/plasticdb_out/out_plasticdb_parsed_best.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID17/interpro_out/CID17_iprout.tsv \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID17/nr_out/out_nr_parsed_best.txt \
/bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID17/antismash_out/index.html
# additional output is available in the respective output directories

# extract ribosomal sequences
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Strain_annotation/CID17/prokka_out
grep "barrnap" CID17.gff | cut -f9 | cut -d';' -f1 | sed 's/ID=//' > tmp_rRNA.accnos
conda activate bbmap-39.01
filterbyname.sh in=CID17.ffn out=../CID17_rRNA.ffn names=tmp_rRNA.accnos include=t ow=t

# Move final data products to dedicated directory for long term archiving
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/
cp Assembly_polishing/CID17/polished_assembly.fa ../Final_results/Assemblies/CID17_polished_assembly.fa
cp -r Strain_annotation/CID17/prokka_out ../Final_results/Gene_prediction/CID17_prokka
rm ../Final_results/Gene_prediction/CID17_prokka/prokka_cds_annotation.txt ../Final_results/Gene_prediction/CID17_prokka/tmp_rRNA.accnos
cp Strain_annotation/CID17/kegg_out/out_kegg_parsed_best.txt ../Final_results/KEGG_annotation/CID17_kegg_parsed_best.txt
cp Strain_annotation/CID17/kofam_out/out_kofam_parsed.txt ../Final_results/KOfam_annotation/CID17_kofam_parsed.txt
cp Strain_annotation/CID17/deeptmhmm_out/CID17_deeptmhmm_summary.txt ../Final_results/TMHMM_annotation/CID17_deeptmhmm_summary.txt
cp Strain_annotation/CID17/interpro_out/CID17_iprout.tsv ../Final_results/IPR_annotation/CID17_iprout.tsv
cp Strain_annotation/CID17/nr_out/out_nr_parsed_best.txt ../Final_results/NR_annotation/CID17_nr_parsed_best.txt
cp -r Strain_annotation/CID17/antismash_out ../Final_results/BGC_annotation/CID17_antismash
