# phylogenomic tree of pigment bearing strains
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/Phylogenomic_trees

# step 1: genome selection

# take genomes associated with phytoene synthase gene
mkdir ref_genomes
cd ref_genomes
cut -f4 ../genome_selection_trees.txt | sed '1d' | parallel -j20 wget {}
gunzip *.gz
# rename to accession number only as file name (also skipping assembly version: always 1)
ls -1 *.fna | cut -d'_' -f1,2 | cut -d'.' -f1 | while read line
do
  mv ${line}* ${line}.fna
done
cd ..
# add CID strains
cp ../Strain_annotation/CID*/prokka_out/CID*.fna ref_genomes

# run gtdb-tk to get taxonomy
conda activate gtdbtk-2.1.0
gtdbtk classify_wf --genome_dir ref_genomes --out_dir ref_gtdbtk -x fna --cpus 96 --pplacer_cpus 10 --min_af 0.5 > gtdbtk.log 2>&1

# prepare custom taxonomy
cut -f1,2 ref_gtdbtk/gtdbtk.bac120.summary.tsv | sed '1d' > crtB_genomes_custom_taxonomy.txt

# GTDB denovo workflow
gtdbtk de_novo_wf --genome_dir ref_genomes --outgroup_taxon p__Proteobacteria --bacteria --custom_taxonomy_file crtB_genomes_custom_taxonomy.txt --out_dir gtdbtk_denovo_out --cpus 96 --extension fna --skip_gtdb_refs --prot_model LG --gamma
# continue to work with rooted, but undecorated tree

# move main output to Final_results
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results
cp -r Phylogenomic_trees ../Final_results/Phylogenomic_tree
rm -rf ../Final_results/Phylogenomic_tree/ref_*