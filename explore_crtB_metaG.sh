# explore crtB occurrence in water and plastic metaG

# OMDv1 assemblies
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/metaG_crtB_comparison/OMDv1_assemblies
# remove contigs shorter than 1000bp and rename fasta headers
conda activate seqkit-2.3.1
ls -1 *.scaffolds.min500.fasta | sed 's/.scaffolds.min500.fasta//' | while read line
do
  seqkit seq -m 1000 -w 0 ${line}.scaffolds.min500.fasta | sed -e '/^>/s/ .*$//' -e 's/^>.*scaffold_/>c/' > ${line}_clean.fasta
done
# run gene prediction
conda activate pprodigal-1.0.1
ls -1 *_clean.fasta | sed 's/_clean.fasta//' | while read line
do
  pprodigal -i ${line}_clean.fasta -p meta -o ${line}.gff -a ${line}_protein.faa -d ${line}_protein.fna -f gff -T 32
done
# run antismash
conda activate antismash-6.1.1
ls -1 *_clean.fasta | sed 's/_clean.fasta//' | parallel -j24 'antismash -c 4 --taxon bacteria --cb-general --cb-knownclusters --cb-subclusters --output-dir antismash_out_{} --genefinding-tool none --genefinding-gff3 {}.gff {}_clean.fasta > antismash_log_{}.log 2>&1'
# extract crtB hits
cut -f2 downloaded/OMD_metaG_selected_results_read_run_tsv.txt | sed '1d' | while read line
do 
  grep -c ">CrtB<" antismash_out_*${line}*/knownclusterblast/region*/*.html | sed 's/:/\t/' | awk '$2 > 0' | cut -f1 | xargs -n1 basename | sed 's/_mibig_hits\.html//' > crtB_hits/${line}_CrtB_hits.txt
done
# get raw reads
cut -f57 ../OMD_metaG_selected_results_read_run_tsv.txt | sed '1d' | tr ';' '\n' | sed 's/^/ftp:\/\//' > downloaded/links
cut -f57 ../OMD_metaG_selected_results_read_run_tsv.txt | sed '1d' | tr ';' '\n' | xargs -n1 basename | paste <(cut -f59 ../OMD_metaG_selected_results_read_run_tsv.txt | sed '1d' | tr ';' '\n') - > downloaded/md5sums.txt
cd downloaded
conda activate aria2-1.34.0
aria2c -i links -c --dir . --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=10 &>> download.log
md5sum -c md5sums.txt > md5sums_checked.txt
cd ..
conda activate qc-env
fastqc -t 32 -o fastqc_out downloaded/*.gz
ls -1 downloaded/*_1.fastq.gz | xargs -n1 basename | sed 's/_1.fastq.gz//' > sample_list.txt
conda activate bbmap-39.01
cat sample_list.txt | while read line
do
  RL=$(readlength.sh in=downloaded/${line}_1.fastq.gz bin=10 max=500 nzo=t reads=1000000 | grep "Mode" | cut -f2)
  if [ $RL -gt 160 ]
  then
    ML=100
  else
    ML=$(echo "scale=0 ; $RL / 3 * 2" | bc)
  fi
  echo $ML
  bbduk.sh in=downloaded/${line}_1.fastq.gz in2=downloaded/${line}_2.fastq.gz out=phix_removed/${line}_1.fastq.gz out2=phix_removed/${line}_2.fastq.gz ref=phix k=28 stats=phix_removed/${line}.stats threads=70 fastawrap=300 > phix_removed/${line}.log 2>&1
  bbduk.sh in=phix_removed/${line}_1.fastq.gz in2=phix_removed/${line}_2.fastq.gz out=adapter_clipped/${line}_1.fastq.gz out2=adapter_clipped/${line}_2.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=adapter_clipped/${line}.stats threads=70 tpe fastawrap=300 > adapter_clipped/${line}.log 2>&1
  bbduk.sh in=adapter_clipped/${line}_1.fastq.gz in2=adapter_clipped/${line}_2.fastq.gz out=clean_reads/${line}_1.fastq.gz out2=clean_reads/${line}_2.fastq.gz qtrim=w,4 trimq=15 minlength=$ML trimpolyg=10 threads=70 fastawrap=300 > clean_reads/${line}.log 2>&1
done
gunzip clean_reads/*.gz
module load metawrap/1.3.2
conda activate metawrap-env
cat sample_list.txt | parallel -j26 'metaWRAP read_qc --skip-trimming --skip-pre-qc-report -1 clean_reads/{}_1.fastq -2 clean_reads/{}_2.fastq -o human_cleaned/{} -t 4 > human_cleaned/{}_bmtagger.log 2>&1'
cat sample_list.txt | while read line
do
  mv human_cleaned/${line}/final_pure_reads_1.fastq human_cleaned/final/${line}_1.fastq
  mv human_cleaned/${line}/final_pure_reads_2.fastq human_cleaned/final/${line}_2.fastq
done
conda activate coverm-0.6.1
cut -f2,124 downloaded/OMD_metaG_selected_results_read_run_tsv.txt | sed '1d' | while read line
do 
  RUN=$(echo "$line" | cut -f2)
  SAM=$(echo "$line" | cut -f1)
  bwa index *${SAM}*clean.fasta
  bwa mem -v 1 -t 60 *${SAM}*clean.fasta human_cleaned/final/${RUN}_1.fastq human_cleaned/final/${RUN}_2.fastq > mapped_reads/${SAM}.sam
  samtools sort -T tmp-samtools -@ 60 -O BAM -o mapped_reads/${SAM}.bam mapped_reads/${SAM}.sam
done
conda activate subread-2.0.1
cut -f2 downloaded/OMD_metaG_selected_results_read_run_tsv.txt | sed '1d' | while read line
do 
  featureCounts --primary -t CDS -g ID -p -D 1000 --minOverlap 50 -T 32 -a *${line}*.gff -o ${line}_genes_counts.txt mapped_reads/${line}.bam > ${line}_feature_counts.log 2>&1
done
# also low mapping rate (except for TARA samples)
# run kofamscan to normalize by single copy marker genes instead of TPM
conda activate kofamscan-1.3.0
cut -f2 downloaded/OMD_metaG_selected_results_read_run_tsv.txt | sed '1d' | while read line
do
  exec_annotation -o kofam_out/${line}_kofam.txt -p /bio/Databases/KOfam/Dec2022/kofamscan/profiles -k /bio/Databases/KOfam/Dec2022/kofamscan/ko_list --tmp-dir=tmp.kofam --cpu 80 -E 0.01 *${line}*_protein.faa
  grep "^\*" kofam_out/${line}_kofam.txt | sed 's/^\* //' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sort -k1,1 -k4,4gr -k5,5g | sort -u -k1,1 --merge > kofam_out/${line}_kofam_parsed.txt
  rm -rf tmp.kofam
done
# parse faa headers
cut -f2 downloaded/OMD_metaG_selected_results_read_run_tsv.txt | sed '1d' | while read line
do
  grep '^>' *${line}*_protein.faa | sed -e 's/ .*ID=/\t/' -e 's/;.*//' -e 's/^>//' > kofam_out/${line}_protein.map
done


# Bryant et al. 2016 metaG (PRJNA318384)
cd /bio/Analysis_data/IOWseq000039_GPGP_wholegenomes/Intermediate_results/metaG_crtB_comparison/PRJNA318384_metaG
conda activate qc-env
fastqc -t 48 -o fastqc_out downloaded/*.gz
ls -1 downloaded/*_1.fastq.gz | xargs -n1 basename | sed 's/_1.fastq.gz//' > sample_list.txt
cat sample_list.txt | while read line
do
  bbduk.sh in=downloaded/${line}_1.fastq.gz in2=downloaded/${line}_2.fastq.gz out=phix_removed/${line}_1.fastq.gz out2=phix_removed/${line}_2.fastq.gz ref=phix k=28 stats=phix_removed/${line}.stats threads=70 fastawrap=300 > phix_removed/${line}.log 2>&1
  bbduk.sh in=phix_removed/${line}_1.fastq.gz in2=phix_removed/${line}_2.fastq.gz out=adapter_clipped/${line}_1.fastq.gz out2=adapter_clipped/${line}_2.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=adapter_clipped/${line}.stats threads=70 tpe fastawrap=300 > adapter_clipped/${line}.log 2>&1
  bbduk.sh in=adapter_clipped/${line}_1.fastq.gz in2=adapter_clipped/${line}_2.fastq.gz out=clean_reads/${line}_1.fastq.gz out2=clean_reads/${line}_2.fastq.gz qtrim=w,4 trimq=15 minlength=100 trimpolyg=10 threads=70 fastawrap=300 > clean_reads/${line}.log 2>&1
done
gunzip clean_reads/*.gz
module load metawrap/1.3.2
conda activate metawrap-env
cat sample_list.txt | parallel -j23 'metaWRAP read_qc --skip-trimming --skip-pre-qc-report -1 clean_reads/{}_1.fastq -2 clean_reads/{}_2.fastq -o human_cleaned/{} -t 4 > human_cleaned/{}_bmtagger.log 2>&1'
cat sample_list.txt | while read line
do
  mv human_cleaned/${line}/final_pure_reads_1.fastq human_cleaned/final/${line}_1.fastq
  mv human_cleaned/${line}/final_pure_reads_2.fastq human_cleaned/final/${line}_2.fastq
done
conda activate spades-3.15.5
cat sample_list.txt | while read line
do
  spades.py -o assemblies/${line}_metaspades --meta -1 human_cleaned/final/${line}_1.fastq -2 human_cleaned/final/${line}_2.fastq -t 70 -m 800 -k 21,33,55,77,99,127 > assemblies/${line}_metaspades.log 2>&1
done
conda activate seqkit-2.3.1
cat sample_list.txt | while read line
do
  seqkit seq -m 1000 -w 0 assemblies/${line}_metaspades/scaffolds.fasta | sed -e '/^>/s/_length.*$//' -e '/^>/s/NODE_/c/' > assemblies/${line}_metaspades/${line}_clean.fasta
done
conda activate pprodigal-1.0.1
cat sample_list.txt | while read line
do
  pprodigal -i assemblies/${line}_metaspades/${line}_clean.fasta -p meta -o assemblies/${line}_metaspades/${line}.gff -a assemblies/${line}_metaspades/${line}_protein.faa -d assemblies/${line}_metaspades/${line}_protein.fna -f gff -T 32
done
conda activate antismash-6.1.1
cat sample_list.txt | parallel -j24 'antismash -c 4 --taxon bacteria --cb-general --cb-knownclusters --cb-subclusters --output-dir antismash_out/antismash_out_{} --genefinding-tool none --genefinding-gff3 assemblies/{}_metaspades/{}.gff assemblies/{}_metaspades/{}_clean.fasta > antismash_log_{}.log 2>&1'
# extract crtB hits
cat sample_list.txt | while read line
do 
  grep -c ">CrtB<" antismash_out/antismash_out_${line}/knownclusterblast/region*/*.html | sed 's/:/\t/' | awk '$2 > 0' | cut -f1 | xargs -n1 basename | sed 's/_mibig_hits\.html//' > crtB_hits/${line}_CrtB_hits.txt
done
conda activate coverm-0.6.1
cat sample_list.txt | while read line
do 
  bwa index assemblies/${line}_metaspades/${line}_clean.fasta
  bwa mem -v 1 -t 60 assemblies/${line}_metaspades/${line}_clean.fasta human_cleaned/final/${line}_1.fastq human_cleaned/final/${line}_2.fastq > assemblies/${line}_metaspades/${line}.sam
  samtools sort -T tmp-samtools -@ 60 -O BAM -o assemblies/${line}_metaspades/${line}.bam assemblies/${line}_metaspades/${line}.sam
done
conda activate subread-2.0.1
cat sample_list.txt | while read line
do 
  featureCounts --primary -t CDS -g ID -p -D 1000 --minOverlap 50 -T 32 -a assemblies/${line}_metaspades/${line}.gff -o assemblies/${line}_metaspades/${line}_genes_counts.txt assemblies/${line}_metaspades/${line}.bam > assemblies/${line}_metaspades/${line}_feature_counts.log 2>&1
done
grep "Successfully assigned alignments" assemblies/SRR34014*/*feature_counts.log
# low mapping rate for features, but should be ok
# run kofamscan to normalize by single copy marker genes instead of TPM
conda activate kofamscan-1.3.0
cat sample_list.txt | while read line
do
  exec_annotation -o kofam_out/${line}_kofam.txt -p /bio/Databases/KOfam/Dec2022/kofamscan/profiles -k /bio/Databases/KOfam/Dec2022/kofamscan/ko_list --tmp-dir=tmp.kofam --cpu 80 -E 0.01 assemblies/${line}_metaspades/${line}_protein.faa
  grep "^\*" kofam_out/${line}_kofam.txt | sed 's/^\* //' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sort -k1,1 -k4,4gr -k5,5g | sort -u -k1,1 --merge > kofam_out/${line}_kofam_parsed.txt
  rm -rf tmp.kofam
done
# parse faa headers
cat sample_list.txt | while read line
do
  grep '^>' assemblies/${line}_metaspades/${line}_protein.faa | sed -e 's/ .*ID=/\t/' -e 's/;.*//' -e 's/^>//' > assemblies/${line}_metaspades/${line}_protein.map
done



# parse TPM (or coverage per Mbp sequencing effort) in R for CrtB
# also parse crtB counts normalized by SCG (median of K06942, K01889, K01887, K01875, K01883, K01869, K01873, K01409, K03106, and K03110 according to Salazar et al. 2019)


# data clean-up
# remove phix and adapter cleaned reads
# gzip clean reads
# remove intermediate assembly files
# after publication, move metadata tables with data selection to Final_results
